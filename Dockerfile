FROM golang:1.17-alpine3.13

ENV USER=appuser
ENV UID=10001
RUN adduser \
    --disabled-password \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

WORKDIR /app
COPY . .
RUN ls -al

RUN GOOS=linux GOARCH=amd64 go build -o zendesk-search cmd/main.go

USER "$USER":"$USER"

ENTRYPOINT [ "/app/zendesk-search" ]
