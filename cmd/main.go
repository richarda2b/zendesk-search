package main

import (
	"fmt"
	"os"

	"gitlab.com/richarda2b/zendesk-search/cli"
	"gitlab.com/richarda2b/zendesk-search/search"
)

func main() {
	sources := []search.DataSource{
		openFile("users", "data/users.json"),
		openFile("tickets", "data/tickets.json"),
		openFile("orgs", "data/organizations.json"),
	}

	coll, err := search.NewCollection(sources...)
	if err != nil {
		fmt.Printf("Something went wrong while loading data collection: %v\n", err)
		os.Exit(1)
	}

	run(cli.App{Collection: coll})
}

func openFile(name, path string) search.DataSource {
	file, err := os.Open(path)
	if err != nil {
		fmt.Printf("Error loading data file: %v\n", err)
		os.Exit(1)
	}
	return search.DataSource{Name: name, Data: file}
}

func run(app cli.App) {
	fmt.Println("Welcome to the Zendesk search tool.\nRun 'help' for usage.")
	for {
		res, err := app.Run(os.Stdin)
		if err != nil {
			fmt.Println("Error processing you request, please try again")
			continue
		}
		fmt.Println(res)
	}
}
