# Zendesk Melbourne - Coding Challenge

**Author:** Richard

## Running the application

### Using docker

Requirements:
  - Docker

```bash
./docker_run.sh
```

### Using `go run`

Requirements:
  - go 1.17

```
  go run ./cmd/...
```

## Usage

Once inside the application, there are four available commands:
  - `fields`
  - `search <index> <field> <text>`
  - `help`
  - `exit`

`fields`: Display the list of searchable fields grouped by inde. For example `users: _id, name`

`search <index> <field> <text>`: Searches for documents that match the input query.
  - `<index>` is the index where the document will be searched, example: `users`
  - `<field>` document field to match the input text against, example: `email`
  - `<text>` is the text to be searched, example: `coffeyrasmussen@flotonic.com`. To search for empty fields use `""`

New Req:
  - Search using multiple terms
  - Multiple terms would be searched using OR operator


`help`: Displays information about the usage

`exit`: Exits the program

By defaul **three** data sources will be loaded:
  - `users`: `data/users.json`
  - `orgs`: `data/organizations.json`
  - `tickets`: `data/tickets.json`
Extra sources can be added by adding a new entry to `search.DataSource` in `cmd/main.go`

## Design decisions
  - Documents with different schemas are allowed within the same data source.
  - Field names and terms are case sensitive in `search` operations
### Indexing documents

There are three main data points used to search/retrieve a given document:
  - `index`: The name of the source where the document was loaded from
  - `field`: The field the input text is matched against
  - `term`: The value of the field

Base on the above, documents are indexed in a "flat" manner using a `key` constructed by concatenating the 3 mentioned values using `<index>::<field>::<term>`. For example, given a document

```json
{"a": 1, "b": 2, "c": 3}
```

Assuming it was loaded from a source named `mysource`, it would be indexed **3** times as:
  - `mysource::a::1` -> doc
  - `mysource::b::2` -> doc
  - `mysource::c::3` -> doc
To reduce the memory usage keys reference a point as opposed to a new copy of the document


## Assumptions, constraints and trade-offs
The following are assumptions and/or trade-offs made in order to reduce the scope and development time:
  - Only the three provided data files are loaded by default.
  - Only data in JSON format can be loaded
  - In the responses for `field` and `search` order of the results is not guaranteed.
  - Couple of tests might occasionally fail due to the results order not being guaranteed.
  - Only allows loading one source per type. For example if another entry `openFile("users", "data/another.json")` was added in `main.go` the data from the file would replace the initial `users` data loaded
  - Individual terms in fields with `list` types are not indexed
  - Formatted responses are not very pretty

## Improvements
  - Caching formatted response: For very long documents, formatted responses could be cached to improve performance
  - The formatting of the responses could use a lot of work
  - Logging and error responses could be improved to enhance the user's/developer's experience
