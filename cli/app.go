package cli

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"

	"gitlab.com/richarda2b/zendesk-search/search"
)

type collection interface {
	Search(search.Query) []*search.Document
	AvailableFields() search.CollectionFields
}

type App struct {
	Collection collection
}

func (app App) Run(input io.Reader) (string, error) {
	const (
		helpAction   = "help"
		searchAction = "search"
		fieldsAction = "fields"
		exitAction   = "exit"
	)

	fmt.Print("> ")
	reader := bufio.NewReader(input)
	inputStr, err := reader.ReadString('\n')
	inputStr = strings.TrimSpace(inputStr)
	if err != nil {
		return "", err
	}

	fields := strings.SplitN(inputStr, " ", 4)
	switch {
	case len(fields) == 1 && fields[0] == helpAction:
		return helpMessage, nil
	case len(fields) == 1 && fields[0] == fieldsAction:
		return app.listFields(), nil
	case len(fields) == 4 && fields[0] == searchAction:
		return app.search(fields), nil
	case len(fields) == 1 && fields[0] == exitAction:
		os.Exit(0)
	}
	return fmt.Sprintf(
		"%v: unknown command\n%v",
		strings.TrimSpace(inputStr),
		invalidCmdMessage), nil
}

func formatSearchResults(docs []*search.Document) string {
	resultBuilder := strings.Builder{}

	formatDoc := func(doc *search.Document) {
		for field, value := range doc.Data {
			resultBuilder.WriteString(fmt.Sprintf("%-20s %v\n", field, value))
		}
	}

	for _, doc := range docs {
		formatDoc(doc)
		resultBuilder.WriteString("\n")
	}

	return resultBuilder.String()
}

func (app App) listFields() string {
	resultBuilder := strings.Builder{}
	for indexName, fields := range app.Collection.AvailableFields() {
		resultBuilder.WriteString(fmt.Sprintf("index: %v\nfields:\n", indexName))

		for _, field := range fields {
			resultBuilder.WriteString(fmt.Sprintf("  - %v\n", field))
		}
	}
	return resultBuilder.String()
}

func (app App) search(queryParts []string) string {
	queryTerms := strings.Split(queryParts[3], "||")
	for i, t := range queryTerms {
		t = strings.TrimSpace(t)
		if t == `""` {
			t = ""
		}
		queryTerms[i] = t
	}

	query := search.Query{
		Index: queryParts[1],
		Field: queryParts[2],
		Term:  queryTerms,
	}

	result := app.Collection.Search(query)

	if len(result) == 0 {
		return fmt.Sprintf(
			"No results found for search query \"%v %v %v\"\n",
			query.Index,
			query.Field,
			query.Term)
	}
	return formatSearchResults(result)
}

const (
	invalidCmdMessage = "Run 'help' for usage."
	helpMessage       = `
zendesk-search is a tool for searching documents
Usage:

		fields
		search <index> <field> <text>
		help
		exit

Description:

		fields		Display the list of searchable fields grouped by index
		search		Searches for documents that match the input query.
				<index> is the index where the document will be searched
				<field> document field to match the input text against
				<text> is the text to be searched.
				To search for empty fields use two double quotes, for example: search users name ""
		help		Displays this message
		exit		Exits the program
`
)
