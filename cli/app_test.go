package cli

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/richarda2b/zendesk-search/search"
)

func TestApp_Search(t *testing.T) {
	tests := []struct {
		name           string
		strQuery       string
		expectedResult string
		coll           collection
		expectedErr    error
	}{
		{
			name:     "with valid query",
			strQuery: "search users field term",
			coll: newStubCollection(map[string]interface{}{
				"_id": 1,
			}, map[string]interface{}{
				"_id": 2,
			}),
			expectedResult: "_id                  1\n\n_id                  2\n\n",
		},
		{
			name:           "with empty result",
			strQuery:       "search users field term",
			coll:           newStubCollection(),
			expectedResult: "No results found for search query \"users field [term]\"\n",
		},
		{
			name:     "with search text with spaces",
			strQuery: "search users field term with space",
			coll: newStubCollection(map[string]interface{}{
				"_id": 1,
			}),
			expectedResult: "_id                  1\n\n",
		},
		{
			name:           "with query too short",
			strQuery:       "search users field",
			coll:           newStubCollection(),
			expectedResult: "search users field: unknown command\nRun 'help' for usage.",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := App{
				Collection: tt.coll,
			}

			reader := strings.NewReader(tt.strQuery + "\n")
			resp, err := a.Run(reader)

			assert.Equal(t, tt.expectedErr, err)
			assert.Equal(t, tt.expectedResult, resp)
		})
	}
}

func TestApp_SearchEmptyField(t *testing.T) {
	coll := newStubCollection()
	var query search.Query
	coll.queryCatch = func(q search.Query) {
		query = q
	}
	a := App{coll}

	_, err := a.Run(strings.NewReader("search foo bar \"\"\n"))

	assert.NoError(t, err)
	assert.Len(t, query.Term, 1)
	assert.Equal(t, "", query.Term[0])
}

func TestApp_SearchMultipleTerms(t *testing.T) {
	coll := newStubCollection()
	var query search.Query
	coll.queryCatch = func(q search.Query) {
		query = q
	}
	a := App{coll}

	_, err := a.Run(strings.NewReader("search foo bar term1 || term2 || \"\"\n"))

	assert.NoError(t, err)
	assert.Len(t, query.Term, 3)
	assert.Equal(t, "term1", query.Term[0])
	assert.Equal(t, "term2", query.Term[1])
	assert.Equal(t, "", query.Term[2])
}
func TestApp_ListField(t *testing.T) {
	a := App{
		stubCollection{fields: map[string][]string{"users": {"foo", "bar"}, "orgs": {"field1", "field2"}}},
	}

	res, err := a.Run(strings.NewReader("fields\n"))

	assert.NoError(t, err)
	assert.Equal(t, "index: users\nfields:\n  - foo\n  - bar\nindex: orgs\nfields:\n  - field1\n  - field2\n", res)
}

func TestApp_Help(t *testing.T) {
	a := App{}

	res, err := a.Run(strings.NewReader("help\n"))

	assert.NoError(t, err)
	assert.Equal(t, helpMessage, res)
}

type stubCollection struct {
	searchResults []*search.Document
	fields        map[string][]string
	queryCatch    func(search.Query)
}

func (c stubCollection) Search(q search.Query) []*search.Document {
	if c.queryCatch != nil {
		c.queryCatch(q)
	}
	return c.searchResults
}

func (c stubCollection) AvailableFields() search.CollectionFields {
	return c.fields
}

func newStubCollection(results ...map[string]interface{}) stubCollection {
	searchResults := make([]*search.Document, len(results))
	for i, r := range results {
		searchResults[i] = &search.Document{Data: r}
	}
	return stubCollection{
		searchResults: searchResults,
	}
}
