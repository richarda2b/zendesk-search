package search

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"testing"
	"testing/iotest"

	"github.com/stretchr/testify/assert"
)

func TestNewCollection(t *testing.T) {
	collectionName := "users"

	t.Run("single document", func(t *testing.T) {
		source := strings.NewReader(`[{"id": 1, "name": "foo", "active": true}]`)
		coll, err := NewCollection(DataSource{Name: collectionName, Data: source})

		assert.NoError(t, err)
		assert.Len(t, coll.data, 3)
		assert.Len(t, coll.data[fmt.Sprintf("%v::%v::%v", collectionName, "id", 1)], 1)
	})

	t.Run("multiple documents", func(t *testing.T) {
		coll, err := NewCollection(
			DataSource{
				Name: collectionName,
				Data: strings.NewReader(`[{"id": 1, "name": "foo"}, {"id": 2}]`),
			},
			DataSource{
				Name: "index2",
				Data: strings.NewReader(`[{"a": 1, "b": "2"}, {"a": 1}]`),
			},
		)

		assert.NoError(t, err)
		assert.Len(t, coll.data, 5)
		assert.Len(t, coll.data[fmt.Sprintf("%v::%v::%v", collectionName, "name", "foo")], 1)
		assert.Len(t, coll.data[fmt.Sprintf("%v::%v::%v", "index2", "a", "1")], 2)
	})

	t.Run("invalid json document", func(t *testing.T) {
		source := strings.NewReader(`[{"id": 1, "name": "foo", "active": true}`)
		coll, err := NewCollection(DataSource{Name: collectionName, Data: source})

		assert.Len(t, coll.data, 0)
		assert.Error(t, err)
	})

	t.Run("error loading source", func(t *testing.T) {
		source := iotest.ErrReader(errors.New("boom"))
		_, err := NewCollection(DataSource{Name: collectionName, Data: source})

		assert.Equal(t, "boom", err.Error())
	})

	t.Run("read from file", func(t *testing.T) {
		source, err := os.Open("../data/users.json")
		defer source.Close()

		if err != nil {
			t.Fatal(err)
		}

		coll, err := NewCollection(DataSource{Name: collectionName, Data: source})

		assert.NoError(t, err)
		assert.Len(t, coll.data[fmt.Sprintf("%v::%v::%v", collectionName, "_id", "1")], 1)
	})
}

func TestQuery(t *testing.T) {
	collectionName := "queryTest"

	t.Run("multiple matching results", func(t *testing.T) {
		coll, err := NewCollection(
			DataSource{
				Name: collectionName,
				Data: strings.NewReader(`[{"id": 1, "name": "foo"}, {"id": 2, "name": "foo"}]`),
			},
			DataSource{
				Name: "index2",
				Data: strings.NewReader(`[{"a": 1, "b": "2"}, {"a": 1}]`),
			},
		)

		q := Query{Index: collectionName, Field: "name", Term: []string{"foo"}}
		docs := coll.Search(q)

		assert.NoError(t, err)
		assert.Len(t, docs, 2)
		assert.Equal(t, docs[0].Data["name"], "foo")
		assert.Equal(t, docs[1].Data["name"], "foo")

		docs = coll.Search(Query{Index: "index2", Field: "b", Term: []string{"2"}})
		assert.Len(t, docs, 1)
		assert.Equal(t, docs[0].Data["b"], "2")
	})

	t.Run("multiple terms", func(t *testing.T) {
		coll, err := NewCollection(
			DataSource{
				Name: collectionName,
				Data: strings.NewReader(`[{"id": 1, "name": "foo"}, {"id": 2, "name": "bar"}]`),
			},
		)

		q := Query{Index: collectionName, Field: "name", Term: []string{"foo", "bar"}}
		docs := coll.Search(q)

		assert.NoError(t, err)
		assert.Len(t, docs, 2)
		assert.Equal(t, docs[0].Data["name"], "foo")
		assert.Equal(t, docs[1].Data["name"], "bar")
	})

	t.Run("empty set of terms", func(t *testing.T) {
		coll, err := NewCollection(
			DataSource{
				Name: collectionName,
				Data: strings.NewReader(`[{"id": 1, "name": "foo"}, {"id": 2, "name": "bar"}]`),
			},
		)

		q := Query{Index: collectionName, Field: "name", Term: []string{}}
		docs := coll.Search(q)

		assert.NoError(t, err)
		assert.Len(t, docs, 0)
	})

	t.Run("empty values", func(t *testing.T) {
		coll := createCollection(t, collectionName, `[{"id": 1, "name": "foo"}, {"id": 2, "name": ""}]`)

		q := Query{Index: collectionName, Field: "name", Term: []string{""}}
		docs := coll.Search(q)

		assert.Len(t, docs, 1)
		assert.Equal(t, docs[0].Data["name"], "")
	})

	t.Run("no matching results", func(t *testing.T) {
		coll := createCollection(t, collectionName, `[{"id": 1, "name": "foo"}, {"id": 2, "name": "foo"}]`)

		q := Query{Index: collectionName, Field: "name", Term: []string{"bar"}}
		docs := coll.Search(q)

		assert.Len(t, docs, 0)
	})

	t.Run("no existing collection", func(t *testing.T) {
		coll := createCollection(t, collectionName, `[{"id": 1, "name": "foo"}, {"id": 2, "name": "foo"}]`)

		q := Query{Index: "invalidCollection", Field: "name", Term: []string{"foo"}}
		docs := coll.Search(q)

		assert.Len(t, docs, 0)
	})

	t.Run("no existing field", func(t *testing.T) {
		coll := createCollection(t, collectionName, `[{"id": 1, "name": "foo"}, {"id": 2, "name": "foo"}]`)

		q := Query{Index: collectionName, Field: "invalidField", Term: []string{"foo"}}
		docs := coll.Search(q)

		assert.Len(t, docs, 0)
	})

	t.Run("sparse field", func(t *testing.T) {
		coll := createCollection(t, collectionName, `[{"id": 1}, {"id": 2}, {"id": 3, "name": "foo"}]`)

		q := Query{Index: collectionName, Field: "name", Term: []string{"foo"}}
		docs := coll.Search(q)

		assert.Len(t, docs, 1)
		assert.Equal(t, docs[0].Data["name"], "foo")
	})
}

func TestListAvailableFields(t *testing.T) {
	t.Run("Empty collection", func(t *testing.T) {
		coll := collection{}
		fields := coll.AvailableFields()

		assert.Len(t, fields, 0)
	})

	t.Run("Multiple fields", func(t *testing.T) {
		coll := createCollection(t, "users", `[{"id": 1}, {"id": 2, "lastname": "nope"}, {"id": 3, "name": "foo"}]`)
		fields := coll.AvailableFields()

		assert.Len(t, fields, 1)
		assert.ElementsMatch(t, fields["users"], []string{"id", "lastname", "name"})
	})

	t.Run("Multiple indexes", func(t *testing.T) {
		coll, err := NewCollection(
			DataSource{
				Name: "users",
				Data: strings.NewReader(`[{"id": 1}, {"id": 2, "lastname": "nope"}, {"id": 3, "name": "foo"}]`),
			},
			DataSource{
				Name: "orgs",
				Data: strings.NewReader(`[{"name": "org1"}, {"address": "somewhere"}]`),
			},
		)
		fields := coll.AvailableFields()

		assert.NoError(t, err)
		assert.Len(t, fields, 2)
		assert.ElementsMatch(t, fields["users"], []string{"id", "lastname", "name"})
		assert.ElementsMatch(t, fields["orgs"], []string{"name", "address"})
	})
}

func createCollection(t *testing.T, collectionName, rawData string) collection {
	source := strings.NewReader(rawData)
	coll, err := NewCollection(DataSource{Name: collectionName, Data: source})
	if err != nil {
		t.Fatal(err)
	}
	return coll
}
