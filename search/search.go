package search

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
)

type Query struct {
	Index string
	Field string
	Term  []string
}

// collection represents a group of indexes grouped by document type
type collection struct {
	data   map[string][]*Document
	fields map[string][]string
}

func (coll collection) Search(q Query) []*Document {
	results := []*Document{}
	for _, t := range q.Term {
		key := buildKey(q.Index, q.Field, t)
		results = append(results, coll.data[key]...)
	}
	return results
}

type CollectionFields map[string][]string

func (coll collection) AvailableFields() CollectionFields {
	return coll.fields
}

func (col collection) indexDocument(index, field, term string, doc *Document) {
	key := buildKey(index, field, term)
	col.data[key] = append(col.data[key], doc)
}

func buildKey(index, field, term string) string {
	return fmt.Sprintf("%v::%v::%v", index, field, term)
}

// Document models a standard json Document
type Document struct {
	Data map[string]interface{}
}

type DataSource struct {
	Name string
	Data io.Reader
}

// NewCollection creates a new collection using the data returned by source.
// source is a reader expected to return JSON data
// name is the type of the documents to be loaded
// NewCollection returns an error if it fails to read from `source` or to parse the data as JSON
func NewCollection(sources ...DataSource) (collection, error) {
	coll := collection{
		data:   make(map[string][]*Document),
		fields: map[string][]string{},
	}
	for _, src := range sources {
		fields, err := indexSource(coll, src)
		if err != nil {
			return collection{}, err
		}
		coll.fields[src.Name] = fields
	}

	return coll, nil
}

func indexSource(coll collection, source DataSource) ([]string, error) {
	data, err := ioutil.ReadAll(source.Data)
	if err != nil {
		return []string{}, err
	}

	jsonData := []map[string]interface{}{}
	if err := json.Unmarshal(data, &jsonData); err != nil {
		return []string{}, err
	}

	uniqFields := map[string]bool{}
	for _, doc := range jsonData {
		for field, term := range doc {
			strTerm := fmt.Sprintf("%v", term)
			coll.indexDocument(source.Name, field, strTerm, &Document{doc})
			uniqFields[field] = true
		}
	}

	fields := []string{}
	for field := range uniqFields {
		fields = append(fields, field)
	}

	return fields, nil
}
