#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail

docker build -t zendesk-search .
docker run -it --rm zendesk-search
